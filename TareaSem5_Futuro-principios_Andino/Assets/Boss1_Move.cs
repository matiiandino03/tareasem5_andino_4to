using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1_Move : StateMachineBehaviour
{
    GameObject player;
    Transform thisBoss;
    Rigidbody2D rb;
    float randX = 0;
    float randY = 0;
    Vector2 newpos;
    Animator animator1;
    float time;
    int randomTime;
    bool isFacingRight = true;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindGameObjectWithTag("Player");
        thisBoss = animator.GetComponent<Transform>();
        rb = animator.GetComponent<Rigidbody2D>();
        animator1 = animator;
        time = 0;
        randX = Random.Range(-7.5f, 7.5f);
        randY = Random.Range(-1.5f, 3f);
        newpos = Vector2.MoveTowards(rb.position, new Vector2(randX, randY), 2.5f * Time.fixedDeltaTime);
        randomTime = Random.Range(3, 8);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        time += Time.deltaTime;
        attackTime();
        if (thisBoss.position.x == randX && thisBoss.position.y == randY)
        {
            randX = Random.Range(-7.5f, 7.5f);
            randY = Random.Range(-3f, 3f);
            newpos = Vector2.MoveTowards(rb.position, new Vector2(randX, randY), 2.5f * Time.fixedDeltaTime);
        }
        else
        {
            newpos = Vector2.MoveTowards(rb.position, new Vector2(randX, randY), 2.5f * Time.fixedDeltaTime);
        }
        rb.MovePosition(newpos);
        Flip();
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    void attackTime()
    {
        if(Mathf.Round(time) == randomTime)
        {
            animator1.SetBool("Move", false);
            animator1.SetBool("Attack", true);
        }
    }

    private void Flip()
    {
        if (isFacingRight && randX < thisBoss.transform.position.x || !isFacingRight  && randX > thisBoss.transform.position.x)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = thisBoss.transform.localScale;
            localScale.x *= -1f;
            thisBoss.transform.localScale = localScale;
            thisBoss.GetComponent<SpriteRenderer>().flipX = !thisBoss.GetComponent<SpriteRenderer>().flipX;
        }
    }

}
