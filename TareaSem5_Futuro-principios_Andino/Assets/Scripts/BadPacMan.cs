using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadPacMan : MonoBehaviour
{
    public float speed = 2.0f;
    private Vector2 direction;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ChangeDirection();
    }

    void Update()
    {
        rb.velocity = transform.right * speed;
        if (HitWall())
        {
            ChangeDirection();
        }
    }

    void ChangeDirection()
    {
        int randomDirection = Random.Range(0, 4);
        switch (randomDirection)
        {
            case 0:
                transform.right = -transform.right;
                break;
            case 1:
                transform.right = -transform.up;
                break;
            case 2:
                transform.right = transform.up;
                break;

        }
    }

    bool HitWall()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, 2.0f);
        return hit.collider != null && hit.collider.CompareTag("Wall");
    }

    //ARREGLAAAAAAAAAAAAAAAAAAAAAAAAR

}

