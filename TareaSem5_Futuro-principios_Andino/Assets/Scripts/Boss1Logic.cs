using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1Logic : MonoBehaviour
{
    Animator animator;

    GameObject player;
    [SerializeField]
    GameObject bullet;
    [SerializeField]
    Transform currentShootDirection;

    bool canShoot = true;
    [SerializeField]
    Image healthbar;
    float life = 100;
    bool attack = false;

    public GameObject Victory;
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        animator.SetBool("Move", true);
    }

    void Update()
    {
        healthbar.fillAmount = life / 100;
        if(animator.GetBool("Attack")==true && canShoot)
        {
            if(attack == false)
            {
                StartCoroutine("Attack");
            }
            else
            {
                transform.right = player.transform.position - transform.position;
                Instantiate(bullet, new Vector3(currentShootDirection.position.x, currentShootDirection.position.y), currentShootDirection.rotation).GetComponent<Rigidbody2D>().velocity = currentShootDirection.right * 8;
                animator.SetBool("Attack", false);
                animator.SetBool("Move", true);
                StartCoroutine("Shootingtimer");
                canShoot = false;
                attack = false;
            }
            
        }
        if(life <= 0)
        {
            animator.SetBool("Attack", false);
            animator.SetBool("Move", false);
            animator.SetBool("Die", true);
            StartCoroutine("Die");
        }
    }

    IEnumerator Shootingtimer()
    {
        yield return new WaitForSeconds(2.8f);
        canShoot = true;
    }
    IEnumerator Die()
    {
        yield return new WaitForSeconds(1f);
        Victory.SetActive(true);
        Destroy(this.gameObject);
    }
    IEnumerator Attack()
    {
        yield return new WaitForSeconds(0.5f);
        attack = true;
    }

    public void GetDamage()
    {
        int damage = Random.Range(2, 4);
        life -= damage;
    }
}
