using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine("DestroyAfterTime");
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Boss1")
        {
            collision.gameObject.GetComponent<Boss1Logic>().GetDamage();
            Destroy(this.gameObject);
        }
        if(collision.gameObject.tag != "Player")
            Destroy(this.gameObject);
    }
    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
