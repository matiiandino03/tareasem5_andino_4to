using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoss1 : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine("DestroyAfterTime");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Physics2D.IgnoreLayerCollision(6, 8, true);
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerController>().GetDamage();
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag != "Boss1" && collision.gameObject.tag != "Bullet")
            Destroy(this.gameObject);
    }
    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
