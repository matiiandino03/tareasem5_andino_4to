using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb;

    public bool onGround = false;

    public PlayerShoot playerShoot;

    public Transform ShootStraight;
    public Transform ShootUp;
    public Transform ShootDiagonalUp;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        playerShoot.currentShootDirection = ShootStraight;
    }

    private void Update()
    {
        #region ShootStill
        if(rb.velocity.x == 0)
        {
            animator.SetBool("RunShooting", false);
            animator.SetBool("RunShootingDiagonalUp", false);
            animator.SetBool("NormalRun", false);
            if (Input.GetKey(KeyCode.X))
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    animator.SetBool("ShootUp", true);
                    playerShoot.currentShootDirection = ShootUp;
                }
                else
                {
                    animator.SetBool("ShootStraight", true);
                    playerShoot.currentShootDirection = ShootStraight;
                }
            }    
            else
            {
                animator.SetBool("ShootStraight", false);
                animator.SetBool("ShootUp", false);
                playerShoot.currentShootDirection = ShootStraight;
            }
        }
        #endregion
        #region Run
        if (rb.velocity.x != 0)
        {
            animator.SetBool("ShootStraight", false);
            animator.SetBool("ShootUp", false);
            if (Input.GetKey(KeyCode.X))
            {
                if((Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow)))
                {
                    if(Input.GetKey(KeyCode.UpArrow))
                    {
                        animator.SetBool("NormalRun", false);
                        animator.SetBool("RunShooting", false);
                        animator.SetBool("RunShootingDiagonalUp", true);
                        playerShoot.currentShootDirection = ShootDiagonalUp;
                    }
                    else
                    {
                        animator.SetBool("NormalRun", false);
                        animator.SetBool("RunShootingDiagonalUp", false);
                        playerShoot.currentShootDirection = ShootStraight;
                        animator.SetBool("RunShooting", true);
                    }
                }
                
            }
            else
            {
                animator.SetBool("NormalRun", true);
            }
        }
        #endregion
        #region Jump
        if (!onGround)
        {
            animator.SetBool("Jump", true);
            animator.SetBool("ShootStraight", false);
            animator.SetBool("ShootUp", false);
            animator.SetBool("RunShooting", false);
            animator.SetBool("RunShootingDiagonalUp", false);
            animator.SetBool("NormalRun", false);
        }
        else
        { animator.SetBool("Jump", false); }
        #endregion

        if(!Input.GetKey(KeyCode.RightArrow)&& !Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.UpArrow)&& !Input.GetKey(KeyCode.DownArrow)&& !Input.GetKey(KeyCode.X))
        {
            animator.SetBool("ShootStraight", false);
            animator.SetBool("ShootUp", false);
            animator.SetBool("RunShooting", false);
            animator.SetBool("RunShootingDiagonalUp", false);
            animator.SetBool("NormalRun", false);
        }
    }
}
