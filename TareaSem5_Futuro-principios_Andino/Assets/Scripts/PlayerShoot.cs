using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform currentShootDirection;
    [SerializeField]
    GameObject bullet;

    bool canShoot = true;

    PlayerController pc;

    private void Start()
    {
        pc = GetComponent<PlayerController>();
    }
    void Update()
    {
        if(Input.GetKey(KeyCode.X))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        if(canShoot)
        {
            if (pc.isFacingRight)
            {
                Instantiate(bullet, new Vector3(currentShootDirection.position.x, currentShootDirection.position.y), currentShootDirection.rotation).GetComponent<Rigidbody2D>().velocity = currentShootDirection.right * 10;
            }
            else
            {
                Instantiate(bullet, new Vector3(currentShootDirection.position.x, currentShootDirection.position.y), currentShootDirection.rotation).GetComponent<Rigidbody2D>().velocity = -currentShootDirection.right * 10;
            }
            canShoot = false;
            StartCoroutine("Shootingtimer");
        }
    }

    IEnumerator Shootingtimer()
    {
        yield return new WaitForSeconds(0.3f);
        canShoot = true;
    }
}
